#pragma once

enum ExpressionType { ET_NumberExpression, ET_ConstantExpression, ET_InvocationExpression, ET_ParenthesisExpression };