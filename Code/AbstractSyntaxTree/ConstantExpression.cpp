#include "Expressions.h"


ConstantExpression::ConstantExpression(int column, std::string identifier)
	 : AstNode(ET_ConstantExpression, column)
{
	identifier_ = identifier;
}

std::string ConstantExpression::ToString(void) const
{
	return identifier_;
}

std::string ConstantExpression::Identifier(void) const
{
	return identifier_;
}