#pragma once

#include "../../stdafx.cpp"
#include "ExpressionType.h"


class AstNode
{
public:
	AstNode(ExpressionType type, int column) : type_(type), column_(column) {};

	virtual std::string ToString(void) const = 0;

	ExpressionType Type(void) const { return type_; };
	int Column(void) const { return column_; };

private:
	ExpressionType type_;
	int column_;
};