#include "Expressions.h"


ParenthesisExpression::ParenthesisExpression(int column, AstNode *expression)
	: AstNode(ET_ParenthesisExpression, column)
{
	expression_ = expression;
}

ParenthesisExpression::~ParenthesisExpression(void)
{
	delete expression_;
}

std::string ParenthesisExpression::ToString(void) const
{
	return "(" + expression_->ToString() + ")";
}

AstNode *ParenthesisExpression::Expression(void) const
{
	return expression_;
}