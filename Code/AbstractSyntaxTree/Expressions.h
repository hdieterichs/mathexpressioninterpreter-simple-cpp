#pragma once

#include "../../StdAfx.cpp"
#include "AstNode.h"


class NumberExpression
	: public AstNode
{
public:
	NumberExpression(int column, double value);

	std::string ToString(void) const;

	double Value(void) const;

private:
	double value_;
};


class ConstantExpression
	: public AstNode
{
public:
	ConstantExpression(int column, std::string identifier);

	std::string ToString(void) const;

	std::string Identifier(void) const;

private:
	std::string identifier_;
};



class InvocationExpression
	: public AstNode
{
public:
	InvocationExpression(int column, std::string method, std::vector<AstNode*> arguments);
	~InvocationExpression(void);

	std::string ToString(void) const;
	ExpressionType Type(void) const;

	int ArgumentCount(void) const;
	AstNode *ArgumentAt(int index) const;

	std::string Method(void) const;

private:
	std::vector<AstNode*> arguments_;
	std::string method_;
};


class BinaryExpression
	: public InvocationExpression
{
public:
	BinaryExpression(int column, std::string method, AstNode *left, AstNode *right);

	std::string ToString(void) const;

	AstNode *Left(void) const;
	AstNode *Right(void) const;
};


class UnaryExpression
	: public InvocationExpression
{
public:
	UnaryExpression(int column, std::string method, AstNode *argument);

	std::string ToString(void) const;

	AstNode *Argument(void) const;
};


class ParenthesisExpression
	: public AstNode
{
public:
	ParenthesisExpression(int column, AstNode *expression);
	~ParenthesisExpression(void);

	std::string ToString(void) const;
	ExpressionType Type(void) const;

	AstNode *Expression(void) const;

private:
	AstNode *expression_;
};
