#include "Expressions.h"


std::vector<AstNode*> GetVector2(AstNode *first, AstNode *second)
{
	std::vector<AstNode*> result;

	result.push_back(first);
	result.push_back(second);

	return result;
}

BinaryExpression::BinaryExpression(int column, std::string method, AstNode *left, AstNode *right)
	: InvocationExpression(column, method, GetVector2(left, right))
{
}

AstNode *BinaryExpression::Left(void) const
{
	return ArgumentAt(0);
}

AstNode *BinaryExpression::Right(void) const
{
	return ArgumentAt(1);
}


std::string BinaryExpression::ToString(void) const
{
	return Left()->ToString() + " " + Method() + " " + Right()->ToString();
}