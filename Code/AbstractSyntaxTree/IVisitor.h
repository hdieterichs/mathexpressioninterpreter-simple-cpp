#pragma once

#include "AstNode.h"
#include "Expressions.h"

template <typename TResult>
class IVisitor
{
public:
	virtual TResult VisitNumberExpression(NumberExpression const *expression) = 0;
	virtual TResult VisitConstantExpression(ConstantExpression const *expression) = 0;
	virtual TResult VisitInvocationExpression(InvocationExpression const *expression) = 0;
	virtual TResult VisitParenthesisExpression(ParenthesisExpression const *expression) = 0;
};

template <class TResult>
TResult VisitAstNode (AstNode const *node, IVisitor<TResult> *visitor) //solved this way, because templated virtual methods are not possible in C++
{
	if (node->Type() == ET_NumberExpression)
		return visitor->VisitNumberExpression((NumberExpression *)node);

	else if (node->Type() == ET_ConstantExpression)
		return visitor->VisitConstantExpression((ConstantExpression *)node);

	else if (node->Type() == ET_InvocationExpression)
		return visitor->VisitInvocationExpression((InvocationExpression *)node);

	else if (node->Type() == ET_ParenthesisExpression)
		return visitor->VisitParenthesisExpression((ParenthesisExpression *)node);

	else
		throw 0; //not supported expression type
}