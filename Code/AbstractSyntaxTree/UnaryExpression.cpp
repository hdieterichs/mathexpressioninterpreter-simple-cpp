#include "Expressions.h"


std::vector<AstNode*> GetVector1(AstNode *first)
{
	std::vector<AstNode*> result;

	result.push_back(first);

	return result;
}

UnaryExpression::UnaryExpression(int column, std::string method, AstNode *argument)
	: InvocationExpression(column, method, GetVector1(argument))
{
}

AstNode *UnaryExpression::Argument(void) const
{
	return ArgumentAt(0);
}

std::string UnaryExpression::ToString(void) const
{
	return Method() + Argument()->ToString();
}