#include "Expressions.h"


NumberExpression::NumberExpression(int column, double value)
	 : AstNode(ET_NumberExpression, column)
{
	value_ = value;
}

std::string NumberExpression::ToString(void) const
{
	std::stringstream NumberString;
	NumberString << value_;
	return NumberString.str();
}

double NumberExpression::Value(void) const
{
	return value_;
}