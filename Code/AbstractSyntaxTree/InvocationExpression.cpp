#include "Expressions.h"


InvocationExpression::InvocationExpression(int column, std::string method, std::vector<AstNode*> arguments)
	: AstNode(ET_InvocationExpression, column)
{
	method_ = method;
	arguments_ = arguments;
}

InvocationExpression::~InvocationExpression(void)
{
	for(std::vector<AstNode*>::const_iterator i = arguments_.begin(); i != arguments_.end(); ++i)
	{
		 delete *i;
	}
}


std::string InvocationExpression::ToString(void) const
{
	std::string result = method_ + "(";

	bool first = true;
	for(std::vector<AstNode*>::const_iterator i = arguments_.begin(); i != arguments_.end(); ++i)
	{
		if (first)
			first = false;
		else
			result += ", ";
		result += (*i)->ToString();
	}

	result += ")";

	return result;
}

int InvocationExpression::ArgumentCount(void) const
{
	return arguments_.size();
}

AstNode *InvocationExpression::ArgumentAt(int index) const
{
	return arguments_.at(index);
}

std::string InvocationExpression::Method(void) const
{
	return method_;
}