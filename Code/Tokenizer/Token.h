#pragma once

#include "../../stdafx.cpp"

#include "TokenType.h"


class Token
{
public:
	Token(int column, std::string symbol, TokenType type);
	Token(const Token& rhs);

	std::string Symbol(void) const;
	TokenType Type(void) const;

	int Column(void) const;

	std::string ToString(void) const;

private:
	std::string symbol_;
	TokenType type_;
	int column_;
};

