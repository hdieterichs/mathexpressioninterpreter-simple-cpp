#include "TokenReader.h"

#include "eTokenException.h"


TokenReader::TokenReader(std::string text)
	: text_(text)
{
	currentToken_ = NULL;
	index_ = -1;
}


TokenReader::~TokenReader(void)
{
	if (currentToken_ != NULL)
	{
		delete currentToken_;
		currentToken_ = NULL;
	}
}


char TokenReader::PeekNextChar(void)
{
	int length = text_.length();
	if (index_ >= length - 1)
		return 0;

	return text_.at(index_ + 1);
}

char TokenReader::ReadNextChar(void)
{
	char c = PeekNextChar();
	index_++;
	return c;
}

Token *TokenReader::NextToken(void)
{
	char c = ' ';

	while (c == ' ')
		c = ReadNextChar();
	
	int column = index_ + 1;

	if (c == 0)
		return new Token(column, "", End);
	else
	{
		std::stringstream ss;
		std::string s;
		ss << c;
		ss >> s; 

		if (c == '(')
			return new Token(column, s, LeftParenthesis);
		else if (c == ')')
			return new Token(column, s, RightParenthesis);
		else if (c == '+' || c == '-' || c == '*' || c == '/')
			return new Token(column, s, ReservedOperator);
		else if (isalpha(c)) //identifier
		{
			while (isalpha(PeekNextChar()))
			{
				char ch = ReadNextChar();
				s += ch;
			}

			return new Token(column, s, Identifier);
		}
		else if (isdigit(c)) //number
		{
			bool decimalPoint = false;
			while (true)
			{
				c = PeekNextChar();
				if (c == '.')
				{
					if (decimalPoint)
						throw eTokenException("Multiple decimal points found", column);
					else
						decimalPoint = true;
				}
				else if (!isdigit(c))
					break;

				s += ReadNextChar();
			}
 
			return new Token(column, s, Number);
		}
		else
			throw eTokenException("Unexpected character found: " + s, column);
	}
}

bool TokenReader::Next(void)
{
	if (currentToken_ != NULL)
		delete currentToken_;
	currentToken_ = NextToken();
	return currentToken_->Type() != End;
}

Token *TokenReader::Peek(void)
{
	int idx = index_;
	Token *token = NextToken();
	index_ = idx;
	return token;
}

Token *TokenReader::CurrentToken(void) const
{
	return currentToken_;
}