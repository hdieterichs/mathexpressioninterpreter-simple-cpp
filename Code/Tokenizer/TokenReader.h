#pragma once

#include "../../StdAfx.cpp"

#include "Token.h"


class TokenReader
{
public:
	TokenReader(std::string text);
	~TokenReader(void);

	bool Next(void);
	Token *CurrentToken(void) const; //Will be disposed automatically

	Token *Peek(void); //Has to be disposed

private:
	std::string text_;
	Token *currentToken_;
	int index_;

	char PeekNextChar(void);
	char ReadNextChar(void);

	Token *NextToken(void);
};

