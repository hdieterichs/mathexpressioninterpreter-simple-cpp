#pragma once

#include "../../StdAfx.cpp"

#include "../eTextException.h"


class eTokenException
	: public eTextException
{
public:
	eTokenException(std::string message, int column)
		: eTextException(message, column)	{	}
};
