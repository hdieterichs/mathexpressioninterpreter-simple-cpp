#pragma once

/*
Defines the type of a token.
Examples:
12.5	: Number
sin		: Identifier
+,*,-,/	: ReservedOperator
*/
enum TokenType { Number, Identifier, LeftParenthesis, RightParenthesis, ReservedOperator, End };
