#include "Token.h"


Token::Token(int column, std::string symbol, TokenType type)
	: column_(column), symbol_(symbol), type_(type)
{

}

Token::Token(const Token& rhs)
	: symbol_(rhs.symbol_), type_(rhs.type_), column_(rhs.column_)
{

}

std::string Token::Symbol(void) const
{
	return symbol_;
}

TokenType Token::Type(void) const
{
	return type_;
}

int Token::Column(void) const
{
	return column_;
}

std::string Token::ToString(void) const
{
	if (type_ == End)
		return "Nothing";

	return symbol_;
}