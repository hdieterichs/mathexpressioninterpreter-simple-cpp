#pragma once

#include "../../StdAfx.cpp"

#include "../eTextException.h"


class eInterpretException 
	: public eTextException 
{
public:
	eInterpretException(std::string message, int column)
		: eTextException(message, column)	{	}
};