
#include "ExpressionInterpreter.h"

#define _USE_MATH_DEFINES
#include <math.h>

#include "eInterpretException.h"


ExpressionInterpreter::ExpressionInterpreter(ILogger *logger)
	: logger_(logger)
{
}


ExpressionInterpreter::~ExpressionInterpreter(void)
{
}


double ExpressionInterpreter::VisitNumberExpression(NumberExpression const *expression)
{
	return expression->Value();
}

double ExpressionInterpreter::VisitConstantExpression(ConstantExpression const *expression)
{
	std::string identifier = expression->Identifier();
	
	if (identifier == "PI")
		return M_PI;
	else if (identifier == "e")
		return M_E;
	throw eInterpretException("Constant '" + identifier + "' not found", expression->Column());
}


double ExpressionInterpreter::VisitInvocationExpression(InvocationExpression const *expression)
{
	std::string method = expression->Method();

	std::string binaryOperators = "+-*/";

	double result = 0;

	if (method == "-" && expression->ArgumentCount() == 1)
	{
		double value = VisitAstNode<double>(expression->ArgumentAt(0), this);
		result = -value;

		std::stringstream ss;
		ss << method << " " << value << " = " << result << std::endl;
		logger_->Log(ss.str());

		return result;
	}

	if (binaryOperators.find(method) != std::string::npos)
	{
		if (expression->ArgumentCount() != 2)
			throw eInterpretException("Invalid argument count for '" + method + "'", expression->Column());

		double left = VisitAstNode<double>(expression->ArgumentAt(0), this);
		double right = VisitAstNode<double>(expression->ArgumentAt(1), this);
	

		if (method == "+")
			result = left + right;
		else if (method == "-")
			result = left - right;
		else if (method == "*")
			result = left * right;
		else if (method == "/")
		{
			if (right == 0)
				throw eInterpretException("Cannot divide by zero", expression->Column());
			result = left / right;
		}

		std::stringstream ss;
		ss << left << " " << method << " " << right << " = " << result << std::endl;
		logger_->Log(ss.str());

		return result;
	}

	if (method == "sin" || method == "tan")
	{
		if (expression->ArgumentCount() != 1)
			throw eInterpretException("Invalid argument count for '" + method + "'", expression->Column());

		double argument = VisitAstNode<double>(expression->ArgumentAt(0), this);

		if (method == "sin")
			result = sin(argument);
		else if (method == "tan")
			result = tan(argument);

		std::stringstream ss;
		ss << method << "(" << argument << ") = " << result << std::endl;
		logger_->Log(ss.str());

		return result;
	}


	throw eInterpretException("Operator '" + method + "' not found", expression->Column());
}

double ExpressionInterpreter::VisitParenthesisExpression(ParenthesisExpression const *expression)
{
	return VisitAstNode<double>(expression->Expression(), this);
}