#pragma once

#include "../../StdAfx.cpp"

#include "../AbstractSyntaxTree/IVisitor.h"
#include "../Logger/ILogger.h"


class ExpressionInterpreter :
	public IVisitor<double>
{
public:
	ExpressionInterpreter(ILogger *logger);
	~ExpressionInterpreter(void);

	double VisitNumberExpression(NumberExpression const *expression);
	double VisitConstantExpression(ConstantExpression const *expression);
	double VisitInvocationExpression(InvocationExpression const *expression);
	double VisitParenthesisExpression(ParenthesisExpression const *expression);

private:
	ILogger *logger_;
};

