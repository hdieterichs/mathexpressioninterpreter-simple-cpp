#pragma once

#include "../../StdAfx.cpp"

#include "ILogger.h"


class ConsoleLogger :
	public ILogger
{
public:
	ConsoleLogger(void);

	void Log(std::string message);

private:
	int step_;
};

