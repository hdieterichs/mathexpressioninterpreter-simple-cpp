#include "ConsoleLogger.h"

ConsoleLogger::ConsoleLogger(void)
{
	step_ = 0;
}

void ConsoleLogger::Log(std::string message)
{
	step_++;
	std::cout << step_ << ". Step: " <<  message;
}