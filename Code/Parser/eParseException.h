#pragma once

#include "../../stdafx.cpp"

#include "../eTextException.h"


class eParseException 
	: public eTextException 
{
public:
	eParseException(std::string message, int column)
		: eTextException(message, column)	{	}
};