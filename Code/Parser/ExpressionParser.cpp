#include "ExpressionParser.h"

#include "eParseException.h"


int GetOperatorPriority(std::string op)
{
	if (op == "+" || op == "-")
		return 50;
	else if (op == "*" || op == "/")
		return 100;

	return 200;
}


ParenthesisExpression *ExpressionParser::ParseParanthesisExpression(TokenReader *tokenReader) //( is already read
{
	AstNode *node = ParseChainedExpression(tokenReader, 0);

	tokenReader->Next();
	Token *token = tokenReader->CurrentToken();

	if (token->Type() != RightParenthesis)
		throw eParseException("Right parenthesis expected, but got " + token->Symbol(), token->Column());

	return new ParenthesisExpression(token->Column(), node);
}

InvocationExpression *ExpressionParser::ParseInvocationExpression(TokenReader *tokenReader, std::string method) //identifier is already read
{
	tokenReader->Next();
	if (tokenReader->CurrentToken()->Type() != LeftParenthesis)
		throw eParseException("Left parenthesis expected, but got " + tokenReader->CurrentToken()->Symbol(), tokenReader->CurrentToken()->Column());

	AstNode *node = ParseChainedExpression(tokenReader, 0);

	tokenReader->Next();
	if (tokenReader->CurrentToken()->Type() != RightParenthesis)
		throw eParseException("Right parenthesis expected, but got " + tokenReader->CurrentToken()->Symbol(), tokenReader->CurrentToken()->Column());

	std::vector<AstNode*> arguments;

	arguments.push_back(node);

	return new InvocationExpression(tokenReader->CurrentToken()->Column(), method, arguments);
}

AstNode *ExpressionParser::ParseTerm(TokenReader *tokenReader)
{
	tokenReader->Next();

	Token token(*tokenReader->CurrentToken());

	if (token.Type() == Identifier)
	{
		std::auto_ptr<Token> nextToken(tokenReader->Peek());
		if (nextToken->Type() == LeftParenthesis)
			return ParseInvocationExpression(tokenReader, token.Symbol());
		else
			return new ConstantExpression(token.Column(), token.Symbol());
	}
	else if (token.Symbol() == "-")
	{
		return new UnaryExpression(token.Column(), token.Symbol(), ParseTerm(tokenReader));
	}
	else if (token.Type() == LeftParenthesis)
		return ParseParanthesisExpression(tokenReader);
	else if (token.Type() == Number)
	{
		double d;			
		std::string text = token.Symbol();
		const char *c_ptr=text.c_str();
		d = atof(c_ptr);				

		return new NumberExpression(token.Column(), d);
	}
	else
		throw eParseException("Identifier, left paranthesis or number expected, but got " + token.ToString(), token.Column());
}

AstNode *ExpressionParser::ParseChainedExpression(TokenReader *tokenReader, int operatorPriority)
{
	AstNode *expression = ParseTerm(tokenReader);

	std::auto_ptr<Token> nextToken(tokenReader->Peek());
	while (nextToken->Type() == ReservedOperator)
	{
		int currentOperatorPriority = GetOperatorPriority(nextToken->Symbol());

		if (currentOperatorPriority <= operatorPriority) //if operatorPriority is 0, this expression is always false.
			break; //If the priority of the next operator is too high to join with this operator, the next operator will be processed later

		tokenReader->Next();
		AstNode *secondExpression = ParseChainedExpression(tokenReader, GetOperatorPriority(nextToken->Symbol()));
		expression = new BinaryExpression(nextToken->Column(), nextToken->Symbol(), expression, secondExpression);
		nextToken.reset(tokenReader->Peek());
	}

	return expression;
}

AstNode *ExpressionParser::Parse(std::string text)
{
	TokenReader tokenReader(text);

	AstNode *result = ParseChainedExpression(&tokenReader, 0);

	if (tokenReader.Next())
		throw eParseException("Unexpected token '" + tokenReader.CurrentToken()->Symbol() + "'", tokenReader.CurrentToken()->Column());

	return result;
}