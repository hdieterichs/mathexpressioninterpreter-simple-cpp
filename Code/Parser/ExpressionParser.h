#pragma once

#include "../../stdafx.cpp"

#include "../Tokenizer/TokenReader.h"
#include "../AbstractSyntaxTree/AstNode.h"
#include "../AbstractSyntaxTree/Expressions.h"


class ExpressionParser
{
public:
	AstNode *Parse(std::string text);

private:
	ParenthesisExpression *ExpressionParser::ParseParanthesisExpression(TokenReader *tokenizer);
	InvocationExpression *ExpressionParser::ParseInvocationExpression(TokenReader *tokenizer, std::string method);
	AstNode *ExpressionParser::ParseTerm(TokenReader *tokenizer);
	AstNode *ExpressionParser::ParseChainedExpression(TokenReader *tokenizer, int operatorPriority);
};

