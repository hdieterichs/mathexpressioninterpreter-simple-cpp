// Program.cpp : The entry point
//

#include "../StdAfx.cpp"

#include "Parser/ExpressionParser.h"
#include "Logger/ConsoleLogger.h"
#include "Interpreter/ExpressionInterpreter.h"
#include "eTextException.h"


int _tmain(int argc, _TCHAR* argv[])
{
	std::cout << "----- Simple Math Expression Interpreter -----" << std::endl;
	std::cout << "--------- By Henning Dieterichs 2012 ---------" << std::endl;
	std::cout << "Please enter an expression like -(-5 + e) * PI" << std::endl << std::endl;

	while (true)
	{
		std::cout << "> ";

		std::string input;
		std::getline(std::cin, input, '\n');
		try
		{
			ExpressionParser parser;
			std::auto_ptr<AstNode> tree(parser.Parse(input));
			std::cout << "Recognized as: " << tree->ToString() << std::endl << std::endl;

			ConsoleLogger logger;
			ExpressionInterpreter interpreter(&logger);
			double result = VisitAstNode<double>(tree.get(), &interpreter);
			std::cout << std::endl << " -> " << result << std::endl;
		}
		catch (const eTextException &exception)
		{
			std::cout << exception.ToString() << std::endl;
		}

		std::cout << std::endl << std::endl << std::endl;
	}

	return 0;
}
