#include "eTextException.h"


std::string eTextException::Message(void) const 
{ 
	return message_; 
};

int eTextException::Column(void) const 
{ 
	return column_; 
};

std::string eTextException::ToString(void) const 
{
	std::stringstream ss;
	ss << message_;
	ss << " in column ";
	ss << column_;

	return ss.str();
};