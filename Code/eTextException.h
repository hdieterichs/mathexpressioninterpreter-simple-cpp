#pragma once

#include "../stdafx.cpp"


class eTextException
{
public:
	eTextException(std::string message, int column) 
		: message_(message), column_(column) {};

	std::string Message(void) const;
	int Column(void) const;

	std::string ToString(void) const;

private:
	std::string message_;
	int column_;
};